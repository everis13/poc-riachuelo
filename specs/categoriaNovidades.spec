# POC Riachuelo - Tela Categoria Novidades



## CT001 - Acessar a tela da categoria Novidades

tags: categoriaNovidades, CT001

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Novidades"

## CT002 - Filtros de subcategoria para categoria Novidades

tags: categoriaNovidades, CT002

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Novidades"
* Deverão ser exibidos os filtros "Tamanho;Gola;Manga;Marca;Padronagem;Personagem;Cor simples;Altura do_salto;Efeito;Enchimento;Finalidade;Modelagem;Tipo de_pele;General Color;Linha Catalogo;Classe Catalogo"

## CT003 - Acessar detalhes do produto quando categoria Novidades

tags: categoriaNovidades, CT003

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Novidades"
* Clicar no primeiro produto exibido na tela
* Deverá exibir o texto "Detalhes"
* Deverá exibir o texto "Mais informações"
* Deverá exibir o texto "Adicionar a Sacola"

## CT004 - Funcionalidade do botão <Adicionar a Sacola> quando categoria Novidades

tags: categoriaNovidades, CT004

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Novidades"
* Clicar no primeiro produto exibido na tela
* Clicar no elemento "Adicionar a Sacola"
* Deverá exibir o texto "Sua sacola"
* Deverá exibir o texto "Total"
* Deverá exibir o botão "Continuar comprando"
* Deverá exibir o botão "Finalizar compra"

