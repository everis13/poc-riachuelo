# POC Riachuelo - Tela Categoria Outlet



## CT001 - Acessar a tela da categoria Outlet

tags: categoriaOutlet, CT001

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Outlet"

## CT002 - Filtros de subcategoria para categoria Outlet

tags: categoriaOutlet, CT002

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Outlet"
* Deverão ser exibidos os filtros "Tamanho;Gola;Manga;Marca;Padronagem;Personagem;Cor simples;Altura do_salto;Camera Frontal;Camera Traseira;Cartao Memoria;Efeito;Enchimento;Finalidade;Memoria Interna;Modelagem;Tamanho da_cama;Tamanho de_tela;Tecnologia Rede;Tipo de_cabelo;Tipo de_pele;Veloc Processamento;General Color;Linha Catalogo;Classe Catalogo"

## CT003 - Acessar detalhes do produto quando categoria Outlet

tags: categoriaOutlet, CT003

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Outlet"
* Clicar no primeiro produto exibido na tela
* Deverá exibir o texto "Detalhes"
* Deverá exibir o texto "Escolha o Tamanho"
* Deverá exibir o texto "Mais informações"
* Deverá exibir o texto "Adicionar a Sacola"

## CT004 - Funcionalidade do botão <Adicionar a Sacola> quando categoria Outlet

tags: categoriaOutlet, CT004

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Outlet"
* Clicar no primeiro produto exibido na tela
* Clicar no elemento "Adicionar a Sacola"
* Deverá exibir o texto "Sua sacola"
* Deverá exibir o texto "Total"
* Deverá exibir o botão "Continuar comprando"
* Deverá exibir o botão "Finalizar compra"

