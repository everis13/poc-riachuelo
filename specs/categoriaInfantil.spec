# POC Riachuelo - Tela Categoria Infantil



## CT001 - Acessar a tela da categoria Infantil

tags: categoriaInfantil, CT001

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Infantil"
* Deverá exibir o texto "Escolha por categoria"
* Deverá exibir o texto "Escolha a subcategoria"

## CT002 - Filtros de categoria para categoria Infantil

tags: categoriaInfantil, CT002

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Infantil"
* Deverão ser exibidos os filtros "Meninas;Meninos;Bebês meninas;Bebês meninos;Garotas;Garotos;Bebê e Mamãe"

## CT003 - Filtros de subcategoria para categoria Infantil

tags: categoriaInfantil, CT003

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Infantil"
* Deverão ser exibidos os filtros "Tamanho;Gola;Manga;Marca;Padronagem;Personagem;Cor Simples;Altura do_salto;Modelagem;General Color;Linha Catalogo;Classe Catalogo"

## CT004 - Acessar detalhes do produto quando categoria Infantil

tags: categoriaInfantil, CT004

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Infantil"
* Clicar no primeiro produto exibido na tela
* Deverá exibir o texto "Detalhes"
* Deverá exibir o texto "Mais informações"
* Deverá exibir o texto "Escolha o tamanho"
* Deverá exibir o texto "Adicionar a Sacola"

## CT005 - Funcionalidade do botão <Adicionar a Sacola> quando categoria Infantil

tags: categoriaInfantil, CT005

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Clicar no elemento "Infantil"
* Clicar no primeiro produto exibido na tela
* Clicar no elemento "Adicionar a Sacola"
* Deverá exibir o texto "Sua sacola"
* Deverá exibir o texto "Total"
* Deverá exibir o botão "Continuar comprando"
* Deverá exibir o botão "Finalizar compra"

