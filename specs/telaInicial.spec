# POC Riachuelo - Tela Inicial



## CT001 - Fazer Login com sucesso

tags: telaInicial, CT001

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Deverá exibir o texto "Logado"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Deverá exibir o texto "Bem-vindo à Riachuelo"

## CT002 - Fazer Login sem informar CPF

tags: telaInicial, CT002

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Clicar no botão "Continuar"
* Deverá exibir o texto "CPF inválido"

## CT003 - Fazer Login sem informar Matrícula

tags: telaInicial, CT003

* Navegar para a loja Riachuelo
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Deverá exibir o texto "Matrícula inválida"

## CT004 - Fazer Login sem informar a loja

tags: telaInicial, CT004

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Deverá exibir o texto "Selecione a loja para prosseguir"

## CT005 - Fazer Login sem informar o dispositivo

tags: telaInicial, CT005

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no botão "Continuar"
* Deverá exibir o texto "Selecione um dispositivo para prosseguir"

## CT006 - Funcionalidade do campo busca da tela inicial

tags: telaInicial, CT006

* Navegar para a loja Riachuelo
* Preencher o textbox "Matrícula" com o valor "3996983"
* Preencher o textbox "CPF" com o valor "01438148690"
* Clicar no botão "Continuar"
* Clicar no elemento "Selecione a loja"
* Clicar no elemento "L002 - SP SAO BERNARDO CAMPO CT"
* Clicar no elemento "Selecione o Dispositivo"
* Clicar no elemento "Macbook"
* Clicar no botão "Continuar"
* Preencher o textbox "Procure aqui" com o valor "camiseta"
* Pressionar a tecla "Enter"
* Deverá exibir o texto "Resultado de busca"


