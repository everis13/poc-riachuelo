/* globals gauge*/
"use strict";
const path = require('path');
const {
    openBrowser,
    write,
    closeBrowser,
    goto,
    press,
    screenshot,
    above,
    click,
    checkBox,
    listItem,
    toLeftOf,
    link,
    text,
    into,
    textBox,
    evaluate,
    dropDown,
    button,
    intercept,
    deleteCookies,
    image
} = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeScenario(async () => {
    await openBrowser({
        headless: headless,
        ignoreCertificateErrors: true,
        args: [
            '--window-size=1920,1080',
        ]
    })
});

afterScenario(async () => {
    await deleteCookies();
    await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
    const screenshotFilePath = path.join(process.env['gauge_screenshots_dir'],
        `screenshot-${process.hrtime.bigint()}.png`);

    await screenshot({
        path: screenshotFilePath
    });
    return path.basename(screenshotFilePath);
};

step("Navegar para a loja Riachuelo", async () => {
    await goto("https://e-store.rchlo.io/", {
        headers:{
            'Authorization':'Basic cG9zdG1hbjpwYXNzd29yZA==',
        }
    });
});
step("Preencher o textbox <campo> com o valor <valor>", async (campo, valor) => {
    await write(valor, into(textBox(campo)));
});
step("Deverá exibir o texto <texto>", async function (texto) {
    assert.strictEqual(true, await text(texto).exists());
});
step("Deverá exibir o botão <btn>", async function (btn) {
    assert.strictEqual(true, await button(btn).exists());
});
step("Selecione a opção <opt> no dropdown <combo>", async function (opt, combo) {
    await dropDown(combo).select(opt);
});
step("Clicar no botão <btn>", async function (btn) {
    await click(button(btn));
});
step("Clicar no botão <btn> interceptando a requisição <url>", async function (btn, url) {
    await intercept(url, {
        success: true
    })
    await click(button(btn))
});
step("Clicar no elemento <el>", async function (el) {
    assert.ok(await text(el).exists());
    await click(el);
});
step("Pressionar a tecla <tecla>", async (tecla) => {
    await press(tecla);
});
step("Deverão ser exibidos os filtros <string>", async (string) => {
    let array = string.split(";");
    for(let i = 0; i < array.length; i++){
        console.log(array[i]);
        assert.ok(await text(array[i]).exists());
    }
});
step("Clicar no primeiro produto exibido na tela", async () => {
    await click(image());
});

