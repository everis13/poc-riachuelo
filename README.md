# Proof of Concept - Riachuello

https://e-store.rchlo.io/

## Spec Tela Inicial (Tag: telaInicial)

| ID do CT | Nome do CT                                    |
| -------- | --------------------------------------------- |
| CT001    | Fazer Login com sucesso                       |
| CT002    | Fazer Login sem informar CPF                  |
| CT003    | Fazer Login sem informar Matrícula            |
| CT004    | Fazer Login sem informar a loja               |
| CT005    | Fazer Login sem informar o dispositivo        |
| CT006    | Funcionalidade do campo busca da tela inicial |

## Spec Categoria Feminino (Tag: categoriaFeminino)

| ID do CT | Nome do CT                                                             |
| -------- | ---------------------------------------------------------------------- |
| CT001    | Acessar a tela da categoria Feminino                                   |
| CT002    | Filtros de categoria para categoria Feminino                           |
| CT003    | Filtros de subcategoria para categoria Feminino                        |
| CT004    | Acessar detalhes do produto quando categoria Feminino                  |
| CT005    | Funcionalidade do botão <Adicionar a Sacola> quando categoria Feminino |

## Spec Categoria Infantil (Tag: categoriaInfantil)

| ID do CT | Nome do CT                                                             |
| -------- | ---------------------------------------------------------------------- |
| CT001    | Acessar a tela da categoria Infantil                                   |
| CT002    | Filtros de categoria para categoria Infantil                           |
| CT003    | Filtros de subcategoria para categoria Infantil                        |
| CT004    | Acessar detalhes do produto quando categoria Infantil                  |
| CT005    | Funcionalidade do botão <Adicionar a Sacola> quando categoria Infantil |

## Spec Categoria Casa (Tag: categoriaCasa)

| ID do CT | Nome do CT                                                         |
| -------- | ------------------------------------------------------------------ |
| CT001    | Acessar a tela da categoria Casa                                   |
| CT002    | Filtros de categoria para categoria Casa                           |
| CT003    | Filtros de subcategoria para categoria Casa                        |
| CT004    | Acessar detalhes do produto quando categoria Casa                  |
| CT005    | Funcionalidade do botão <Adicionar a Sacola> quando categoria Casa |

## Spec Categoria Novidades (Tag: categoriaNovidades)

| ID do CT | Nome do CT                                                              |
| -------- | ----------------------------------------------------------------------- |
| CT001    | Acessar a tela da categoria Novidades                                   |
| CT002    | Filtros de subcategoria para categoria Novidades                        |
| CT003    | Acessar detalhes do produto quando categoria Novidades                  |
| CT004    | Funcionalidade do botão <Adicionar a Sacola> quando categoria Novidades |

## Spec Categoria Outlet (Tag: categoriaOutlet)

| ID do CT | Nome do CT                                                           |
| -------- | -------------------------------------------------------------------- |
| CT001    | Acessar a tela da categoria Outlet                                   |
| CT002    | Filtros de subcategoria para categoria Outlet                        |
| CT003    | Acessar detalhes do produto quando categoria Outlet                  |
| CT004    | Funcionalidade do botão <Adicionar a Sacola> quando categoria Outlet |
